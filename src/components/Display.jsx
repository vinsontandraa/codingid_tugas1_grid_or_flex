import React from 'react'

const Display = () => {
    return (
        <div className='grid-container'>
            <div className='square grid-items item1'></div>
            <div className='triangle grid-items item2'></div>
            <div className='vroundsquare grid-items item3'></div>
            <div className='roundsquare grid-items item4'></div>
            <div  className='round grid-items item5'></div>   
            <div className='longsquare grid-items item6'></div>
        </div>
    )
}

export default Display;